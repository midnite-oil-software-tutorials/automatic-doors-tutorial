using System;
using UnityEngine;
using UnityEngine.UIElements;

public class AutomaticDoors : MonoBehaviour
{
    Animator _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _animator.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ToggleAnimation();
        }
    }

    void ToggleAnimation()
    {
        if (_animator.enabled)
        {
            _animator.enabled = false;
            ResetDoors();
            return;
        }

        _animator.enabled = true;
    }

    void ResetDoors()
    {
        Transform door = transform.GetChild(0);
        var position = door.position;
        position.y = -0.5f;
        door.position = position;
        door = transform.GetChild(1);
        position = door.position;
        position.y = 0.5f;
        door.position = position;
    }
}
