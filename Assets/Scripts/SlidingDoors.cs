using System;
using UnityEngine;

public class SlidingDoors : MonoBehaviour
{
    Animator _animator;
    static readonly int OpenDoors = Animator.StringToHash("OpenDoors");
    static readonly int CloseDoors = Animator.StringToHash("CloseDoors");

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _animator.enabled = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            _animator.SetTrigger(OpenDoors);
        }

        else if (Input.GetKeyDown(KeyCode.C))
        {
            _animator.SetTrigger(CloseDoors);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        _animator.SetTrigger(OpenDoors);
    }

    void OnTriggerExit2D(Collider2D other)
    {
        _animator.SetTrigger(CloseDoors);
    }
}
